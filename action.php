<?php
/**
 * Code copy to clipboard Action Plugin:
 *
 * @author     Sebastien Lamy <seb@sldja.com>
 */

if(!defined('DOKU_INC')) die();
        
class action_plugin_codeclipboard extends DokuWiki_Action_Plugin {
     
    public function register(Doku_Event_Handler $controller) {
        $controller->register_hook('TPL_METAHEADER_OUTPUT', 'BEFORE', $this,
                                   '_hookjs');
    }
     
    public function _hookjs(Doku_Event $event, $param) {    }
}
