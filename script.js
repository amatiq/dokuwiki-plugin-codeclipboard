jQuery(function () {

  jQuery( 'pre.code' ).each( function() {

    var $elmt = jQuery( this );

    // Add button
   	$btn = jQuery( '<div class="codeclipboard_button"></div>' );

    $elmt.prepend($btn);

    // Setup click behaviour
    $btn.click(function() {
      var $txt = jQuery( '<textarea />' );
	  $txt.val($elmt.text()).css({ width: "1px", height: "1px" }).appendTo('body');
	  $txt.select();
      document.execCommand('copy');
      $txt.remove();
    });

  });

});
